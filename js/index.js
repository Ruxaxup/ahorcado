// Como bd.js se ejecutó primero que index.js, tenemos acceso a la constante "paises"
// Respetar esta constante
const Configuraciones = {
    vidas: 5,
    palabra: '',
    posicionesTotales: 0,
    posicionesAcertadas: 0,
    horaInicio: null,
    horaFinal: null
}

// INICIO :: Crear todas las funciones correspondientes al juego
//  == NO MOVER ==
function habilitarBotones() {
    // No modificar esta función
    const botones = document.getElementsByClassName('btn-letra');

    for(let i = 0; i < botones.length; i++) {
        botones[i].disabled = false;
        botones[i].classList.remove('btn-success');
        botones[i].classList.remove('btn-danger');
    }
}

function getRandomElementFromArray(array = []) {
    // No modificar esta función
    return array[Math.floor(Math.random() * array.length)];
}

function cambiarImagen(id, etapa) {
    // No modificar esta función
    if(etapa > 5) {
        etapa = 5;
    } else if(etapa < 0) {
        etapa = 0;
    }

    document.getElementById(id).src = `img/${etapa}.png`;
}

function generarPalabra() {
    return getRandomElementFromArray(paises);
}

function getDiffBetweenDatesInMinutes() {
    // No modificar esta función
    const horaInicio = Juego.horaInicio.getTime();
    const horaFinal = Juego.horaFinal.getTime();
    const diff = horaFinal - horaInicio;

    return {
        minutos: Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60)),
        segundos: Math.floor((diff % (1000 * 60)) / 1000)
    }
}
//  == NO MOVER ==

function inicializarJuego() {
    Juego.vidas = 5;
    Juego.palabra = generarPalabra();
    Juego.posicionesAcertadas = 0;
    Juego.posicionesTotales = 0;
    Juego.horaInicio = new Date();

    // Crear una funcion para generar los espacios de las letras
    // generarEspaciosParaLetras();

    cambiarImagen('horca', 0);

    // Mostrar imagen de la Horca
    // Mostrar teclas
    // Mostrar letras
    // Mostrar el botón para cambiar de palabra
    // Esconder botón para comenzar
    // Esconder pantalla de finalización
}

function generarEspaciosParaLetras() {
    let html = ''

    // Obtenemos un arreglo con cada letra de la palabra
    // Chile nos regresaria => ['C', 'h', 'i', 'l', 'e']
    // El Salvador nos regresaria => ['E', 'l', ' ', 'S', 'a', 'l', 'v', 'a', 'd', 'o', 'r']
    const palabrasSeparadas = Juego.palabra.split('');

    // Aqui tenemos que generar los espacios para las letras segun el caracter actual
    // Si el caracter es un espacio, mostrar un espacio en blanco
    // Si el caracter es una letra, mostrar un guion bajo

    document.getElementById('letras').innerHTML = html;
}

function onClickTeclado(event, letra) {
    // Buscar las posiciones de la letra en la palabra
    // Sustituir los guiones bajos de los espacios por la letra en dischas posiciones
    // Si el numero de posiciones encontradas es igual a cero, descontar una vida
    // Si el numero de posiciones encontradas es al menos 1, aumentamos el numero de posiciones acertadas
    // Deshabilitar el boton utilizado y cambiar el color del boton a rojo si no acertó y a verde si acertó
    // Siempre revisar el estatus del juego para saber si ya finalizó
}

function revisarEstatusJuego() {
    // Si la cantidad de vidas es igual a cero, mostrar mensaje de perdio y reiniciar el juego
    // Si la cantidad de posiciones acertadas es igual al numero de posiciones totales, mostrar mensaje de ganó y reiniciar el juego
    // Esconder todo el juego y mantner el botón de iniciar
    // Mostrar el mensaje de perdio o ganó
    // Opcional:  Mostrar cuanto tiempo demoró el jugador en finalizar el juego, usar función getDiffBetweenDatesInMinutes
}
// FIN :: funciones